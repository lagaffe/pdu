# Description
Management interface for a Linksys PDU ...

The library argparse was taken without any modification from https://argparse.readthedocs.io/en/stable/index.html

# Pre-requisite
the system should have :
* lua 5.3
* snmp commands

# Installation
1. clone the repository
2. copy 'config.lua.example' in 'config.lua' and adapt outlet names

# Usage
* getting help:
```
pdu -h
```
* getting help for a specific command ```pdu <command> -h```. Example:
```
pdu get -h
```
