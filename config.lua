CONFIG={
  address = "10.254.254.2",
  community = {
    read = "public",
    write = "pdu"
  }
}


local getSimple = function(addr,cmty,oid)
  local params="-v 1 -c " .. cmty .. " " .. addr

  local ptr=assert(io.popen('snmpget ' .. params .. ' ' .. oid, 'r'))
  local resp = assert(ptr:read('*a'))
  ptr:close()

  local _,_,datatype=string.find(resp,".*%s=%s(%a*):%s.*")
  if datatype == 'INTEGER' then
    _,_,v=string.find(resp,".*:%s(%d*)%c")
    val=tonumber(v)
  elseif datatype == 'STRING' then
    _,_,val=string.find(resp,".*:%s\"(.*)\"%c")
  elseif datatype == 'Timeticks' then
    _,_,val=string.find(resp,".*:%s%((.*)%).*")
  else
    _,_,val=string.find(resp,".*:%s\"(.*)\".*")
  end

  return val
end

local setSimple = function(addr,cmty,oid,val)
  local params="-v 1 -c " .. cmty .. " " .. addr
  local ptr=assert(io.popen('snmpset ' .. params .. ' ' .. oid .. ' s ' .. val, 'w'))
  local resp = assert(ptr:read('*a'))
  ptr:close()

  return val
end


local getOutletName = function(addr,cmty,oid)
  local fullName = "undef,0,0,0,0"

  fullName = getSimple(addr,cmty,oid)
  if fullName then
    _,_,n=string.find(fullName,"(%a*),.*")
    return n
  else
    return "NotFound"
  end
end


local getOutletStatus = function(addr,cmty,oid,pos)
  local ostat
  local statusStr = "a,b,c,d,e,f,g,h"

  statusStr = OBJECT_LIST['status']['get']()

  if statusStr then
    local c=string.sub(statusStr,pos,pos)
    if c == '0' then
      ostat='OFF'
    elseif c == '1' then
      ostat='ON'
    else
      ostat="UNDEF(" .. c .. ")"
    end
  else
    ostat="NotFound"
  end
  return ostat
end

local setOutletStatus = function(addr,cmty,oid,pos,val)
  local v
  local currentStatus="a,b,c,d,e,f,g,h"
  local newStatus

  currentStatus=OBJECT_LIST['status']['get']()

  if val == 'OFF' or val == '0' then
    v='0'
  elseif val == 'ON' or val == '1' then
    v='1'
  else
    return 'error', "only 0,1,ON or OFF are allowed, but '" .. (val or 'nil') .. "' found"
  end

  if pos == 1 then
    newStatus=v .. string.sub(currentStatus,2)
  else
    newStatus=string.sub(currentStatus,1,pos-1) .. v .. string.sub(currentStatus,pos+1)
  end
  
  return OBJECT_LIST['status']['set'](newStatus)
end


-- snmpwalk -c public -v 1 10.254.254.2 iso
OBJECT_LIST={
  uptime = {
    get = function () return getSimple(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.2.1.1.3.0') end,
    display = function (val) return "uptime  " .. (val or "NotFound") .. "ms" end
  },
  poweroutlet = {
    get = function () return getSimple(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.4.1.17420.1.2.9.1.11.0') end,
    display = function (val) return "power  " .. (val or "NotFound") .. "dA" end
  },
  powerpdu = {
    get = function () return getSimple(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.4.1.17420.1.2.9.1.12.0') end,
    display = function (val) return "powerPDU  " .. (val or "NotFound") .. "dA" end
  },
  status = {
    get = function () return getSimple(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.4.1.17420.1.2.9.1.13.0') end,
    set = function (v) return setSimple(CONFIG['address'],CONFIG['community']['write'],'iso.3.6.1.4.1.17420.1.2.9.1.13.0',v) end,
    display = function (val) return "status  " .. (val or "NotFound") end
  },
  outleta = {
    get = function () return getOutletStatus(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.4.1.17420.1.2.9.1.14.1.0',1) end,
    set = function (v) return setOutletStatus(CONFIG['address'],CONFIG['community']['write'],'iso.3.6.1.4.1.17420.1.2.9.1.14.1.0',1,v) end,
    display = function (val) return "outleta  " .. (val or "NotFound") end
  },
  outletb = {
    get = function () return getOutletStatus(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.4.1.17420.1.2.9.1.14.2.0',3) end,
    set = function (v) return setOutletStatus(CONFIG['address'],CONFIG['community']['write'],'iso.3.6.1.4.1.17420.1.2.9.1.14.1.0',3,v) end,
    display = function (val) return "outletb  " .. (val or "NotFound") end
  },
  outletc = {
    get = function () return getOutletStatus(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.4.1.17420.1.2.9.1.14.3.0',5) end,
    set = function (v) return setOutletStatus(CONFIG['address'],CONFIG['community']['write'],'iso.3.6.1.4.1.17420.1.2.9.1.14.1.0',5,v) end,
    display = function (val) return "outletc  " .. (val or "NotFound") end
  },
  outletd = {
    get = function () return getOutletStatus(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.4.1.17420.1.2.9.1.14.4.0',7) end,
    set = function (v) return setOutletStatus(CONFIG['address'],CONFIG['community']['write'],'iso.3.6.1.4.1.17420.1.2.9.1.14.1.0',7,v) end,
    display = function (val) return "outletd  " .. (val or "NotFound") end
  },
  outlete = {
    get = function () return getOutletStatus(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.4.1.17420.1.2.9.1.14.5.0',9) end,
    set = function (v) return setOutletStatus(CONFIG['address'],CONFIG['community']['write'],'iso.3.6.1.4.1.17420.1.2.9.1.14.1.0',9,v) end,
    display = function (val) return "outlete  " .. (val or "NotFound") end
  },
  outletf = {
    get = function () return getOutletStatus(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.4.1.17420.1.2.9.1.14.6.0',11) end,
    set = function (v) return setOutletStatus(CONFIG['address'],CONFIG['community']['write'],'iso.3.6.1.4.1.17420.1.2.9.1.14.1.0',11,v) end,
    display = function (val) return "outletf  " .. (val or "NotFound") end
  },
  outletg = {
    get = function () return getOutletStatus(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.4.1.17420.1.2.9.1.14.7.0',13) end,
    set = function (v) return setOutletStatus(CONFIG['address'],CONFIG['community']['write'],'iso.3.6.1.4.1.17420.1.2.9.1.14.1.0',13,v) end,
    display = function (val) return "outletg  " .. (val or "NotFound") end
  },
  outleth = {
    get = function () return getOutletStatus(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.4.1.17420.1.2.9.1.14.8.0',15) end,
    set = function (v) return setOutletStatus(CONFIG['address'],CONFIG['community']['write'],'iso.3.6.1.4.1.17420.1.2.9.1.14.1.0',15,v) end,
    display = function (val) return "outleth  " .. (val or "NotFound") end
  },
  outleta_name = {
    get = function () return getOutletName(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.4.1.17420.1.2.9.1.14.1.0') end,
    set = function (v) return setSimple(CONFIG['address'],CONFIG['community']['write'],'iso.3.6.1.4.1.17420.1.2.9.1.14.1.0',v) end,
    display = function (val) return "outleta  " .. (val or "NotFound") end
  },
  outletb_name = {
    get = function () return getOutletName(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.4.1.17420.1.2.9.1.14.2.0') end,
    set = function (v) return setSimple(CONFIG['address'],CONFIG['community']['write'],'iso.3.6.1.4.1.17420.1.2.9.1.14.2.0',v) end,
    display = function (val) return "outletb  " .. (val or "NotFound") end
  },
  outletc_name = {
    get = function () return getOutletName(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.4.1.17420.1.2.9.1.14.3.0') end,
    set = function (v) return setSimple(CONFIG['address'],CONFIG['community']['write'],'iso.3.6.1.4.1.17420.1.2.9.1.14.3.0',v) end,
    display = function (val) return "outletc  " .. (val or "NotFound") end
  },
  outletd_name = {
    get = function () return getOutletName(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.4.1.17420.1.2.9.1.14.4.0') end,
    set = function (v) return setSimple(CONFIG['address'],CONFIG['community']['write'],'iso.3.6.1.4.1.17420.1.2.9.1.14.4.0',v) end,
    display = function (val) return "outletd  " .. (val or "NotFound") end
  },
  outlete_name = {
    get = function () return getOutletName(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.4.1.17420.1.2.9.1.14.5.0') end,
    set = function (v) return setSimple(CONFIG['address'],CONFIG['community']['write'],'iso.3.6.1.4.1.17420.1.2.9.1.14.5.0',v) end,
    display = function (val) return "outlete  " .. (val or "NotFound") end
  },
  outletf_name = {
    get = function () return getOutletName(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.4.1.17420.1.2.9.1.14.6.0') end,
    set = function (v) return setSimple(CONFIG['address'],CONFIG['community']['write'],'iso.3.6.1.4.1.17420.1.2.9.1.14.6.0',v) end,
    display = function (val) return "outletf  " .. (val or "NotFound") end
  },
  outletg_name = {
    get = function () return getOutletName(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.4.1.17420.1.2.9.1.14.7.0') end,
    set = function (v) return setSimple(CONFIG['address'],CONFIG['community']['write'],'iso.3.6.1.4.1.17420.1.2.9.1.14.7.0',v) end,
    display = function (val) return "outletg  " .. (val or "NotFound") end
  },
  outleth_name = {
    get = function () return getOutletName(CONFIG['address'],CONFIG['community']['read'],'iso.3.6.1.4.1.17420.1.2.9.1.14.8.0') end,
    set = function (v) return setSimple(CONFIG['address'],CONFIG['community']['write'],'iso.3.6.1.4.1.17420.1.2.9.1.14.8.0',v) end,
    display = function (val) return "outleth  " .. (val or "NotFound") end
  }
}
